// Copyright 2021 The HDF5-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	"modernc.org/ccgo/v3/lib"
)

const (
	tarFile = tarName + ".tar.gz"
	tarName = tarDir
	tarDir  = "hdf5-hdf5-1_12_1"
)

type supportedKey = struct{ os, arch string }

var (
	gcc       = ccgo.Env("GO_GENERATE_CC", "gcc")
	goarch    = ccgo.Env("TARGET_GOARCH", runtime.GOARCH)
	goos      = ccgo.Env("TARGET_GOOS", runtime.GOOS)
	supported = map[supportedKey]struct{}{
		{"linux", "amd64"}: {},
	}
	tmpDir = ccgo.Env("GO_GENERATE_TMPDIR", "")
)

func main() {
	if _, ok := supported[supportedKey{goos, goarch}]; !ok {
		ccgo.Fatalf(true, "unsupported target: %s/%s", goos, goarch)
	}

	ccgo.MustMkdirs(true,
		"lib",
		"libhl",
	)
	if tmpDir == "" {
		tmpDir = ccgo.MustTempDir(true, "", "go-generate-")
		defer os.RemoveAll(tmpDir)
	}
	srcDir := tmpDir + "/" + tarDir
	os.RemoveAll(srcDir)
	ccgo.MustUntarFile(true, tmpDir, tarFile, nil)
	cdb, err := filepath.Abs(tmpDir + "/cdb.json")
	if err != nil {
		ccgo.Fatal(true, err)
	}

	cc, err := exec.LookPath(gcc)
	if err != nil {
		ccgo.Fatal(true, err)
	}

	os.Setenv("CC", cc)
	cfg := []string{
		"--enable-build-mode=production",
		"--enable-shared=no",
	}
	switch goos {
	case "linux":
		// nop
	}
	if _, err := os.Stat(cdb); err != nil {
		if !os.IsNotExist(err) {
			ccgo.Fatal(true, err)
		}

		ccgo.MustInDir(true, srcDir, func() error {
			ccgo.MustShell(true, "./configure", cfg...)
			ccgo.MustCompile(true, "-compiledb", cdb, "make")
			return nil
		})
	}
	ccgo.MustCompile(true,
		"-export-defines", "",
		"-export-enums", "",
		"-export-externs", "X",
		"-export-fields", "F",
		"-export-structs", "",
		"-export-typedefs", "",
		"-o", filepath.Join("lib", fmt.Sprintf("hdf5_%s_%s.go", goos, goarch)),
		"-pkgname", "hdf5",
		"-trace-translation-units",
		cdb, "libhdf5.a",
	)
	ccgo.MustCompile(true,
		"-export-defines", "",
		"-export-enums", "",
		"-export-externs", "X",
		"-export-fields", "F",
		"-export-structs", "",
		"-export-typedefs", "",
		"-o", filepath.Join("libhl", fmt.Sprintf("hl_%s_%s.go", goos, goarch)),
		"-pkgname", "hl",
		"-trace-translation-units",
		cdb, "libhdf5_hl.a",
	)
}
