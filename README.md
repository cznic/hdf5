# hdf5

HDF5 is a data model, library, and file format for storing and managing data.

It supports an unlimited variety of datatypes, and is designed for flexible and
efficient I/O and for high volume and complex data. HDF5 is portable and is
extensible, allowing applications to evolve in their use of HDF5. The HDF5
Technology suite includes tools and applications for managing, manipulating,
viewing, and analyzing data in the HDF5 format.

## Installation

    $ go get modernc.org/hdf5

## Documentation

[godoc.org/modernc.org/hdf5](http://godoc.org/modernc.org/hdf5)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2hdf5](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fhdf5)
